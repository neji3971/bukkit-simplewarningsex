package de.Neji3971.SimpleWarnings;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

public class SQL_siwa{

	Connection			conn;
	public final String	adress;
	public final String	user;
	public final String	pass;
	public final String	database;
	public final String	prefix;
	public final int	port;

	public SQL_siwa(String adress, String user, String pass, int port, String database, String prefix){
		try{
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}
		this.adress = adress;
		this.user = user;
		this.pass = pass;
		this.port = port;
		this.database = database;
		this.prefix = prefix;
	}

	public void executeUpdate(String query){
		query = query.replace("warnings", prefix + "_warnings");
		query = query.replace("players", prefix + "_players");
		String url = "jdbc:mysql://" + adress + ":" + port + "/" + database;
		try{
			if(conn == null || !conn.isValid(1))conn = DriverManager.getConnection(url, user, pass);
			conn.createStatement().executeUpdate(query);
		}catch(SQLException e){
			e.printStackTrace();
		}
	}

	public ResultSet executeQuery(String query){
		query = query.replace("warnings", prefix + "_warnings");
		query = query.replace("players", prefix + "_players");
		String url = "jdbc:mysql://" + adress + ":" + port + "/" + database;
		ResultSet result = null;
		try{
			if(conn == null || !conn.isValid(1))conn = DriverManager.getConnection(url, user, pass);
			PreparedStatement statement = conn.prepareStatement(query);
			result = statement.executeQuery();
		}catch(Exception e){
			Siwa.log(Level.SEVERE, "Connection to SQL Database not possible!");
			e.printStackTrace();
		}
		return result;
	}
}