package de.Neji3971.SimpleWarnings;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class WarnStatsThread implements Runnable{

	private final CommandSender	sender;

	public WarnStatsThread(CommandSender sender){
		this.sender = sender;
	}

	private void warnstats(CommandSender sender){
		if(sender.hasPermission("SimpleWarnings.warnstats") || sender.isOp()){
			sender.sendMessage(Siwa.parseCC(Siwa.language.getString("stats")));
			List<String> players = new ArrayList<String>();
			ResultSet res = Siwa.sql.executeQuery("SELECT DISTINCT uuid FROM players JOIN warnings ON players.id=warnings.warned_player_id");
			while(true){
				try{
					if(res.next())players.add(res.getString("uuid"));
					else break;
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
			for(int i = 0; i < players.size(); i++){
				int active_warns = 0;
				int all_warns = 0;
				String name = Siwa.getNameFromUUID(UUID.fromString(players.get(i)));
				int ID = Siwa.getIDOfPlayer(players.get(i));
				ResultSet res_active = Siwa.sql.executeQuery("SELECT COUNT(id) AS count_warns FROM warnings WHERE warned_player_id = '" + ID + "' AND active = '1'");
				ResultSet res_all = Siwa.sql.executeQuery("SELECT COUNT(id) AS count_warns FROM warnings WHERE warned_player_id = '" + ID + "'");
				try{
					if(res_active.next())active_warns = res_active.getInt("count_warns");
					if(res_all.next())all_warns = res_all.getInt("count_warns");
				}catch(SQLException e){
					e.printStackTrace();
				}
				if(all_warns > 0)sender.sendMessage(ChatColor.GRAY + "" + (i + 1) + ". " + ChatColor.WHITE + name + ": " + ChatColor.YELLOW + Siwa.getViolationLevel(players.get(i)) + ChatColor.GRAY + ";"
							+ ChatColor.RED + active_warns + ChatColor.GRAY + ";" + ChatColor.DARK_RED + all_warns);
			}
		}else sender.sendMessage(Siwa.parseCC(Siwa.language.getString("no-perm").replace("[PERM]", "SimpleWarnings.warnstats")));
	}

	@Override
	public void run(){
		warnstats(sender);
	}
}