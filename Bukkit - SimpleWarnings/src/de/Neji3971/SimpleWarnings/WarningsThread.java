package de.Neji3971.SimpleWarnings;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.bukkit.command.CommandSender;

public class WarningsThread implements Runnable{

	private final CommandSender	sender;
	private final String		player;
	private final int			ID;

	public WarningsThread(CommandSender sender, String player, int ID){
		this.sender = sender;
		this.player = player;
		this.ID = ID;
	}

	private void getWarnings(CommandSender sender, String player, int ID){
		if(ID == 0){
			String uuid = Siwa.getUUIDFromName(player);
			if(uuid == null)sender.sendMessage(Siwa.parseCC(Siwa.language.getString("warn-added-fail")));
			else{
				int warns = 0;
				int level = Siwa.getViolationLevel(uuid);
				String text = "Something went wrong";
				ResultSet res = Siwa.sql.executeQuery("SELECT COUNT(reason) AS warnings_count FROM warnings WHERE active = '1' AND warned_player_id = '" + Siwa.getIDOfPlayer(uuid) + "'");
				try{
					if(res.next()) warns = res.getInt(1);
				}catch(SQLException e){
					e.printStackTrace();
				}
				if(warns == 0)text = Siwa.parseCC(Siwa.language.getString("sum-warns.none").replace("[PLAYER]", player));
				else if(warns == 1)text = Siwa.parseCC(Siwa.language.getString("sum-warns.one").replace("[PLAYER]", player).replace("[LEVEL]", new Integer(level).toString()));
				else text = Siwa.parseCC(Siwa.language.getString("sum-warns.more").replace("[PLAYER]", player).replace("[LEVEL]", new Integer(level).toString())).replace("[COUNT]", new Integer(warns).toString());
				sender.sendMessage(text);
				sender.sendMessage(Siwa.parseCC(Siwa.language.getString("detailed-info-allwarns")));
			}
		}else{
			String uuid = Siwa.getUUIDFromName(player);
			if(uuid == null)sender.sendMessage(Siwa.parseCC(Siwa.language.getString("warn-added-fail")));
			else{
				ResultSet res = Siwa.sql.executeQuery("SELECT reason,level,active FROM warnings WHERE warned_player_id = " + Siwa.getIDOfPlayer(uuid));
				while(true){
					try{
						if(res.getRow() == ID){
							String reason = res.getString("reason");
							int level = res.getInt("level");
							boolean active = res.getBoolean("active");
							sender.sendMessage(Siwa.parseCC(Siwa.language.getString("out-warn").replace("[ID]", new Integer(ID).toString()).replace("[PLAYER]", player)).replace("[REASON]", reason).replace("[LEVEL]", new Integer(level).toString()).replace("[STATE]", new Boolean(active).toString()));
							break;
						}else if(!res.next())sender.sendMessage(Siwa.parseCC(Siwa.language.getString("warn-not-exist")));
					}catch(SQLException e){
						e.printStackTrace();
					}
				}
			}
		}
	}

	@Override
	public void run(){
		getWarnings(sender, player, ID);
	}
}