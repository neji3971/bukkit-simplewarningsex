package de.Neji3971.SimpleWarnings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

public class Siwa extends JavaPlugin{

	File							configFile			= new File(getDataFolder(), "config.yml");
	File							languageFile, lang_EN, lang_DE;
	public static FileConfiguration	config, language;
	static SQL_siwa					sql;
	private static final Logger		logger				= Logger.getLogger("minecraft");
	boolean							awaitingResponse	= false;

	@Override
	public void onEnable(){
		try{
			Metrics metrics = new Metrics(this);
			metrics.start();
		}catch(IOException e){}
		lang_EN = new File(getDataFolder(), "lang" + File.separator + "English.yml");
		lang_DE = new File(getDataFolder(), "lang" + File.separator + "German.yml");
		YamlConfiguration temp = new YamlConfiguration();
		try{
			temp.load(lang_EN);
			if(temp.get("version") == null || !temp.getString("version").equals("3.1")){
				copy(getResource("English.yml"), lang_EN);
				log("Recreating English language-file (newer version)");
			}
			temp.load(lang_DE);
			if(temp.get("version") == null || !temp.getString("version").equals("3.1")){
				copy(getResource("German.yml"), lang_DE);
				log("Recreating German language-file (newer version)");
			}
		}catch(FileNotFoundException e2){
		}catch(IOException e2){
		}catch(InvalidConfigurationException e2){}
		try{
			firstRun();
		}catch(Exception y){
			y.printStackTrace();
		}
		config = new YamlConfiguration();
		try{
			config.load(configFile);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}catch(InvalidConfigurationException e){
			e.printStackTrace();
		}
		config.addDefault("language", "English");
		config.addDefault("welcome-message", true);
		config.addDefault("broadcast-warning", false);
		config.addDefault("join-prevent", true);
		config.addDefault("prevent-level", 10);
		config.addDefault("run-command", true);
		config.addDefault("command", "jail [PLAYER] r:\"You got a warning-level of at least 5\"");
		config.addDefault("command-level", 5);
		config.addDefault("run-timeout-command", true);
		config.addDefault("timeout-command", "unjail [PLAYER]");
		config.addDefault("timeout", 96);
		config.addDefault("sql.server", "127.0.0.1");
		config.addDefault("sql.port", 3306);
		config.addDefault("sql.database", "simplewarnings");
		config.addDefault("sql.user", "siwa");
		config.addDefault("sql.password", "siwa");
		config.addDefault("sql.table-prefix", "siwa");
		config.addDefault("violations.1.text", "Griefing");
		config.addDefault("violations.1.level", 3);
		config.addDefault("violations.2.text", "Base-Camping");
		config.addDefault("violations.2.level", 1);
		try{
			config.save(configFile);
		}catch(IOException e1){
			e1.printStackTrace();
		}
		sql = new SQL_siwa(config.getString("sql.server"), config.getString("sql.user"), config.getString("sql.password"), config.getInt("sql.port"), config.getString("sql.database"), config.getString("sql.table-prefix"));
		sql.executeUpdate("CREATE TABLE IF NOT EXISTS `players` (`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto-generated ID of the player', `uuid` varchar(36) CHARACTER SET ascii COLLATE ascii_bin NOT NULL COMMENT 'The UUID of the player', `last known name` varchar(32) NOT NULL COMMENT 'The last username that the player with this UUID used', PRIMARY KEY (id))");
		sql.executeUpdate("CREATE TABLE IF NOT EXISTS `warnings` (`id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto-generated ID of that warning', `warned_player_id` int(11) NOT NULL COMMENT 'ID of the warned player', `reason` varchar(128) NOT NULL COMMENT 'The reason for the warning', `level` int(11) NOT NULL COMMENT 'The violation-level of that warning', `issued_player_id` int(11) NOT NULL COMMENT 'ID of the player that issued the warning', `timestamp` bigint(20) NOT NULL COMMENT 'The timestamp from System.currentTimeMillis()', `time_active` int(11) NOT NULL COMMENT 'The time this warning is active in hours', `active` tinyint(1) NOT NULL COMMENT 'If the warning is active or not', `told` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'If the player has seen this warning', PRIMARY KEY (id))");
		setupLanguage();
		getServer().getPluginManager().registerEvents(new ListenerHandler(), this);
		Timer timeoutChecker = new Timer();
		timeoutChecker.schedule(new TimerTask(){
			@Override
			public void run(){
				long time = System.currentTimeMillis();
				if(config.getBoolean("run-timeout-command")){
					ResultSet res = sql.executeQuery("SELECT uuid FROM players");
					List<String> banned_players = new ArrayList<String>();
					while(true){
						try{
							if(res.next()){
								if(getViolationLevel(res.getString("uuid")) >= config.getInt("command-level"))banned_players.add(res.getString("uuid"));
							}else break;
						}catch(SQLException e){
							e.printStackTrace();
						}
					}
					sql.executeUpdate("UPDATE `warnings` SET `active` = '0' WHERE (timestamp<" + time + "-(time_active*3600000)) AND active='1'");
					for(int i = 0; i < banned_players.size(); i++){
						if(getViolationLevel(banned_players.get(i)) < config.getInt("command-level"))Bukkit.dispatchCommand(Bukkit.getConsoleSender(), config.getString("timeout-command").replace("[PLAYER]", getNameFromUUID(UUID.fromString(banned_players.get(i)))));
					}
				}else sql.executeUpdate("UPDATE `warnings` SET `active` = '0' WHERE (timestamp<" + time + "-(time_active*3600000)) AND active='1'");
			}
		}, 1000, 30000);
		if(!Bukkit.getOnlineMode())log(Level.WARNING, "This Plugin cannot work properly on a server running in offline-mode unless you have a bungeecord-server and the Proxy is running in online-mode, then it is totally fine!");
	}

	@Override
	public void onDisable(){}

	private void firstRun() throws Exception{
		if(!configFile.exists()){
			configFile.getParentFile().mkdirs();
			copy(getResource("config.yml"), configFile);
		}if(!lang_EN.exists()){
			lang_EN.getParentFile().mkdirs();
			copy(getResource("English.yml"), lang_EN);
		}if(!lang_DE.exists()){
			lang_DE.getParentFile().mkdirs();
			copy(getResource("German.yml"), lang_DE);
		}
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if(!awaitingResponse){
			if(cmd.getName().equalsIgnoreCase("warn")){
				if(sender.hasPermission("SimpleWarnings.warn") || sender.isOp()){
					if(args.length == 2){
						int Violation_ID = 0;
						try{
							Violation_ID = Integer.parseInt(args[1]);
						}catch(NumberFormatException e){
							sender.sendMessage(parseCC(language.getString("ID-Error")));
							return true;
						}
						addWarning(args[0], config.getString("violations." + Violation_ID + ".text"), config.getInt("violations." + Violation_ID + ".level"), sender);
						return true;
					}else if(args.length < 2)sender.sendMessage(parseCC(language.getString("few-args")));
					else if(args.length > 2)sender.sendMessage(parseCC(language.getString("many-args")));
				}else{
					sender.sendMessage(parseCC(language.getString("no-perm").replace("[PERM]", "SimpleWarnings.warn")));
					return true;
				}
			}
			if(cmd.getName().equalsIgnoreCase("customwarn")){
				if(sender.hasPermission("SimpleWarnings.customwarn") || sender.isOp()){
					if(args.length >= 3){
						String warntext = "";
						for(int i = 2; i < args.length; i++)warntext += args[i] + " ";
						int level = 0;
						try{
							level = Integer.parseInt(args[1]);
						}catch(NumberFormatException ex){
							sender.sendMessage(parseCC(language.getString("level-Error")));
							return true;
						}
						addWarning(args[0], warntext, level, sender);
						return true;
					}else if(args.length < 3)sender.sendMessage(parseCC(language.getString("few-args")));
				}else{
					sender.sendMessage(parseCC(language.getString("no-perm").replace("[PERM]", "SimpleWarnings.customwarn")));
					return true;
				}
			}
			if(cmd.getName().equalsIgnoreCase("warnings") || cmd.getName().equalsIgnoreCase("warns")){
				if(args.length == 0){
					if(sender instanceof Player){
						getWarningsString(sender, sender.getName());
						return true;
					}else return false;
				}else if((sender.hasPermission("SimpleWarnings.warnings") || sender.isOp()) && args.length > 0){
					if(args.length == 1){
						getWarningsString(sender, args[0]);
						return true;
					}else if(args.length == 2){
						int IDp = 0;
						try{
							IDp = Integer.parseInt(args[1]);
						}catch(NumberFormatException e){
							sender.sendMessage(parseCC(language.getString("ID-Error")));
						}
						getWarningsString(sender, args[0], IDp);
						return true;
					}else if(args.length > 2)sender.sendMessage(parseCC(language.getString("many-args")));
					return false;
				}else{
					sender.sendMessage(parseCC(language.getString("no-perm").replace("[PERM]", "SimpleWarnings.warnings")));
					return true;
				}
			}
			if(cmd.getName().equalsIgnoreCase("allwarnings") || cmd.getName().equalsIgnoreCase("allwarns") || cmd.getName().equalsIgnoreCase("listwarns")
					|| cmd.getName().equalsIgnoreCase("listwarnings")){
				if(args.length == 0){
					if(sender instanceof Player){
						getAllWarnings(sender.getName(), sender);
						return true;
					}else return false;
				}else if((sender.hasPermission("SimpleWarnings.allwarnings") || sender.isOp()) && args.length > 0){
					if(args.length == 1){
						getAllWarnings(args[0], sender);
						return true;
					}else if(args.length > 1) sender.sendMessage(parseCC(language.getString("many-args")));
				}else{
					sender.sendMessage(parseCC(language.getString("no-perm").replace("[PERM]", "SimpleWarnings.allwarnings")));
					return true;
				}
			}
			if(cmd.getName().equalsIgnoreCase("deletewarning") || cmd.getName().equalsIgnoreCase("delwarn") || cmd.getName().equalsIgnoreCase("deletewarn")
					|| cmd.getName().equalsIgnoreCase("delwarning")){
				if(sender.hasPermission("SimpleWarnings.deletewarning") || sender.isOp()){
					if(args.length == 2){
						int ID = 0;
						try{
							ID = Integer.parseInt(args[1]);
						}catch(NumberFormatException e){
							sender.sendMessage(parseCC(language.getString("ID_Error")));
						}
						boolean deleted = deleteWarning(args[0], ID);
						if(deleted) sender.sendMessage(parseCC(language.getString("warn-deleted")));
						else sender.sendMessage(parseCC(language.getString("not-deleted")));
						return true;
					}else if(args.length > 2)sender.sendMessage(parseCC(language.getString("many-args")));
					else if(args.length < 2)sender.sendMessage(parseCC(language.getString("few-args")));
					return false;
				}else{
					sender.sendMessage(parseCC(language.getString("no-perm").replace("[PERM]", "SimpleWarnings.deletewarning")));
					return true;
				}
			}
			if(cmd.getName().equalsIgnoreCase("warnstats") || cmd.getName().equalsIgnoreCase("warnstatistics") || cmd.getName().equalsIgnoreCase("warningstats")
					|| cmd.getName().equalsIgnoreCase("warningstatistics")){
				new Thread(new WarnStatsThread(sender)).start();
				return true;
			}
			if(cmd.getName().equalsIgnoreCase("SimpleWarnings") || cmd.getName().equalsIgnoreCase("siwa")){
				if(args.length == 1){
					if(args[0].equalsIgnoreCase("reload") && (sender.hasPermission("SimpleWarnings.reload") || sender.isOp())){
						setupLanguage();
						sender.sendMessage(ChatColor.GREEN + "Reload successful!");
						return true;
					}
				}else if(args.length == 0){
					try{
						sender.sendMessage("§l§eSimpleWarningsEx Version " + this.getPluginLoader().getPluginDescription(getFile()).getVersion());
					}catch(InvalidDescriptionException e){
						e.printStackTrace();
					}
					sender.sendMessage("§8_____________________________________________________");
					sender.sendMessage("§9Help:");
					sender.sendMessage("§6/siwa reload §f- §7Lade die Config und Sprachdateien neu.");
					sender.sendMessage("§6/warn [Spieler] [Katalog-ID] §f- §7Warne einen Spieler.\n§e[Spieler]§7: Der Spieler, der gewarnt werden soll.\n§e[Katalog-ID]§7: Die ID der in der Config vordefinierten Warnung.");
					sender.sendMessage("§6/cwarn [Spieler] [Level] [Grund] §f- §7Verteile eine Warnung, die nicht im Katalog steht.\n§e[Spieler]§7: Der Spieler, der gewarnt werden soll.\n§e[Level]§7: Die Schwere der Warnung.\n§e[Grund]§7: Der Grund für die Warnung.");
					sender.sendMessage("§6/warns <Spieler> <ID> §f- §7Zeige alle Warnungen von dir selbst.\n§eMit <Spieler> definiert§7: Zeige alle Warnungen eines Spielers.\n§eMit <ID> definiert§7: Zeige Details über eine bestimmte Warnung.");
					sender.sendMessage("§6/delwarn [Spieler] [ID] §f- §7Lösche eine Warnung.\n§e[Spieler]§7: Der Spieler, dessen Warnung gelöscht werden soll.\n§e[ID]§7: Die ID der Warnung, die gelöscht werden soll.");
					sender.sendMessage("§6/allwarns <Spieler> §f- §7Zeige alle Warnungen detailliert von dir selbst.\n§eMit <Spieler> definiert§7: Zeige alle Warnungen detailliert von einem Spieler.");
					sender.sendMessage("§6/warnstats §f- §7Zeige Statistiken aller Spieler, die schon Warnungen bekommen haben.");
					sender.sendMessage("§6/pwarns §f- §7Lösche alle inaktiven Warnungen.");
					sender.sendMessage("§6/uuid [player] §f- §7Zeige die UUID eines Spielers.\n§e[Spieler]§7: Der Spieler, dessen UUID angezeigt werden soll.");
					sender.sendMessage("§6/username [UUID] §f- §7Zeige den Nutzernamen hinter einer UUID.\n§e[UUID]§7: Die UUID, die in einen Usernamen konvertiert werden soll.");
					sender.sendMessage("§6/warnlist §f- §7Zeige die Liste aller in der Config vordefinierten Warnungen.");
					return true;
				}else sender.sendMessage(parseCC(language.getString("many-args")));
			}
			if((cmd.getName().equalsIgnoreCase("purgewarnings") || cmd.getName().equalsIgnoreCase("purgewarns") || cmd.getName().equalsIgnoreCase("pwarns"))
					&& sender.hasPermission("SimpleWarnings.purge")){
				sender.sendMessage(parseCC(language.getString("purge-warn")));
				awaitingResponse = true;
				return true;
			}
			if(cmd.getName().equalsIgnoreCase("uuid") && (sender.hasPermission("SimpleWarnings.uuid") || sender.isOp())){
				if(args.length == 1){
					try{
						sender.sendMessage(parseCC(language.getString("uuid")).replace("[UUID]", getUUIDFromName(args[0])).replace("[PLAYER]", args[0]));
					}catch(Exception e){
						sender.sendMessage(parseCC(language.getString("uuid-fail")));
					}
					return true;
				}else if(args.length < 1)sender.sendMessage(parseCC(language.getString("few-args")));
				else sender.sendMessage(parseCC(language.getString("many-args")));
			}
			if(cmd.getName().equalsIgnoreCase("username") && (sender.hasPermission("SimpleWarnings.username") || sender.isOp())){
				if(args.length == 1){
					try{
						sender.sendMessage(parseCC(language.getString("name")).replace("[PLAYER]", getNameFromUUID(UUID.fromString(args[0]))).replace("[UUID]", args[0]));
					}catch(Exception e){
						sender.sendMessage(parseCC(language.getString("name-fail")));
					}
					return true;
				}else if(args.length < 1)sender.sendMessage(parseCC(language.getString("few-args")));
				else sender.sendMessage(parseCC(language.getString("many-args")));
			}
			if(cmd.getName().equalsIgnoreCase("warnlist") && (sender.hasPermission("SimpleWarnings.warnlist") || sender.isOp())){
				if(args.length  == 0){
					sender.sendMessage(parseCC(language.getString("catalogue")));
					for(int i = 1; config.contains("violations." + i); i++){
						sender.sendMessage(parseCC("&e" + i + "&7: &e" + config.getString("violations." + i + ".text") + " &7(&e" + config.getInt("violations." + i + ".level") + "&7)"));
					}
					return true;
				}else{
					sender.sendMessage(parseCC(language.getString("many-args")));
				}
			}
		}else{
			if(!cmd.getName().equalsIgnoreCase("purgeconfirm"))sender.sendMessage(parseCC(language.getString("not-confirmed")));
			else new Thread(new Purger(sender)).start();
			awaitingResponse = false;
			return true;
		}
		return false;
	}

	public static void addWarning(String player, String reason, int level, CommandSender sender){
		new Thread(new addWarningThread(player, reason, level, sender)).start();
	}

	public static int getViolationLevel(String uuid){
		int level = 0;
		ResultSet res = sql.executeQuery("SELECT SUM(level) AS sum_level FROM warnings WHERE active = '1' AND warned_player_id = " + getIDOfPlayer(uuid));
		try{
			if(res.next())level = res.getInt("sum_level");
		}catch(SQLException e){
			e.printStackTrace();
		}
		return level;
	}

	public static int getViolationLevel(int ID){
		int level = 0;
		ResultSet res = sql.executeQuery("SELECT SUM(level) AS sum_level FROM warnings WHERE active = '1' AND warned_player_id = " + ID);
		try{
			if(res.next())level = res.getInt("sum_level");
		}catch(SQLException e){
			e.printStackTrace();
		}
		return level;
	}

	public void getWarningsString(CommandSender sender, String player){
		new Thread(new WarningsThread(sender, player, 0)).start();
	}

	public void getWarningsString(CommandSender sender, String player, int ID){
		new Thread(new WarningsThread(sender, player, ID)).start();
	}

	public void getAllWarnings(String player, CommandSender sender){
		new Thread(new AllWarningsThread(player, sender)).start();
	}

	public static boolean deleteWarning(String player, int ID){
		boolean deleted = false;
		String uuid = getUUIDFromName(player);
		boolean banned = false;
		if(getViolationLevel(uuid) >= config.getInt("command-level")) banned = true;
		ResultSet res = sql.executeQuery("SELECT id FROM warnings WHERE warned_player_id = " + getIDOfPlayer(uuid));
		int id = 0;
		while(true){
			try{
				if(res.next()){
					id++;
					if(id == ID) break;
				}else{
					deleted = false;
					break;
				}
			}catch(SQLException e){}
		}
		try{
			sql.executeUpdate("DELETE FROM warnings WHERE id = '" + res.getInt("id") + "'");
			deleted = true;
		}catch(SQLException e){}
		if(banned && config.getBoolean("run-timeout-command") && getViolationLevel(uuid) < config.getInt("command-level"))Bukkit.dispatchCommand(Bukkit.getConsoleSender(), config.getString("timeout-command").replace("[PLAYER]", getNameFromUUID(UUID.fromString(uuid))));
		return deleted;
	}

	public static String parseCC(String in){
		in = ChatColor.translateAlternateColorCodes('&', in);
		return in;
	}

	public static void log(String msg){
		logger.info("[siwa] " + msg);
	}

	public static void log(Level level, String msg){
		logger.log(level, "[siwa] " + msg);
	}

	private void copy(InputStream in, File file){
		try{
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while((len = in.read(buf)) > 0)out.write(buf, 0, len);
			out.close();
			in.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void setupLanguage(){
		language = new YamlConfiguration();
		languageFile = new File(getDataFolder(), "lang" + File.separator + config.getString("language") + ".yml");
		try{
			language.load(languageFile);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public static String getUUIDFromName(String name){
		Player player = Bukkit.getPlayer(name);
		if(player == null){
			return Bukkit.getOfflinePlayer(name).getUniqueId().toString();
		}else{
			return player.getUniqueId().toString();
		}
	}

	public static String getNameFromUUID(UUID uuid){
		Player player = Bukkit.getPlayer(uuid);
		if(player == null){
			return Bukkit.getOfflinePlayer(uuid).getName();
		}else{
			return player.getName();
		}
	}

	public static int getIDOfPlayer(String uuid){
		int ID = 0;
		ResultSet res = sql.executeQuery("SELECT id FROM players WHERE uuid = '" + uuid + "'");
		while(true){
			try{
				if(res.next())ID = res.getInt("id");
				else break;
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		if(ID == 0){
			String player = getNameFromUUID(UUID.fromString(uuid));
			sql.executeUpdate("INSERT INTO `players` (`id`, `uuid`, `last known name`) VALUES (NULL, '" + uuid + "', '" + player + "')");
			res = sql.executeQuery("SELECT id FROM players WHERE uuid = '" + uuid + "'");
			ID = 0;
			while(true){
				try{
					if(res.next())ID = res.getInt("id");
					else break;
				}catch(SQLException e){
					e.printStackTrace();
				}
			}
		}
		return ID;
	}
}