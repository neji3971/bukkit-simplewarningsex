package de.Neji3971.SimpleWarnings;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.bukkit.command.CommandSender;

public class Purger implements Runnable{

	private final CommandSender	sender;

	public Purger(CommandSender sender){
		this.sender = sender;
	}

	public void purgeWarnings(CommandSender sender){
		int deleted = 0;
		ResultSet res = Siwa.sql.executeQuery("SELECT COUNT(id) AS count_id FROM warnings WHERE active = '0'");
		try{
			if(res.next())deleted = res.getInt("count_id");
		}catch(SQLException e){
			e.printStackTrace();
		}
		Siwa.sql.executeUpdate("DELETE FROM warnings WHERE active = '0'");
		sender.sendMessage(Siwa.parseCC(Siwa.language.getString("purge-confirm")).replace("[COUNT]", new Integer(deleted).toString()));
	}

	@Override
	public void run(){
		purgeWarnings(sender);
	}
}