package de.Neji3971.SimpleWarnings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class addWarningThread implements Runnable{

	private final String		player;
	private final String		reason;
	private final int			level;
	private final CommandSender	sender;

	public addWarningThread(String player, String reason, int level, CommandSender sender){
		this.player = player;
		this.reason = reason;
		this.level = level;
		this.sender = sender;
	}

	public void addWarning(final String player, String reason, int level, CommandSender sender){
		boolean banned = false;
		String uuid = Siwa.getUUIDFromName(player);
		if(uuid == null)sender.sendMessage(Siwa.parseCC(Siwa.language.getString("warn-added-fail")));
		else{
			if(Siwa.getViolationLevel(uuid) >= Siwa.config.getInt("command-level"))banned = true;
			int id = 0;
			int issued = 0;
			id = Siwa.getIDOfPlayer(uuid);
			if(sender instanceof Player)issued = Siwa.getIDOfPlayer(Siwa.getUUIDFromName(sender.getName()));
			else issued = 0;
			Siwa.sql.executeUpdate("INSERT INTO `warnings` (`id`, `warned_player_id`, `reason`, `level`, `issued_player_id`, `timestamp`, `time_active`, `active`) VALUES (NULL, " + id + ", '"
						+ reason + "', " + level + ", " + issued + ", " + System.currentTimeMillis() + ", " + Siwa.config.getInt("timeout") * level + ", 1)");
			sender.sendMessage(Siwa.parseCC(Siwa.language.getString("warn-added")));
			Collection<? extends Player> list = Bukkit.getServer().getOnlinePlayers();
			List<String> listS = new ArrayList<String>();
			for(Player p : list)listS.add(p.getName());
			if(listS.contains(player)){
				Bukkit.getServer().getPlayer(player).sendMessage(Siwa.parseCC(Siwa.language.getString("receive-warn-on")));
				if(Siwa.getViolationLevel(uuid) >= Siwa.config.getInt("prevent-level") && !Bukkit.getServer().getPlayer(player).hasPermission("SimpleWarnings.override")
						&& Siwa.config.getBoolean("join-prevent")){
					Bukkit.getScheduler().runTask(Siwa.getPlugin(Siwa.class), new Runnable(){
						@Override
						public void run() {
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "kick " + player + " " + Siwa.parseCC(Siwa.language.getString("no-join")));
						}
					});
				}
			}
			if(Siwa.config.getBoolean("run-command")){
				if(!banned && Siwa.getViolationLevel(uuid) >= Siwa.config.getInt("command-level")){
					Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(), Siwa.config.getString("command").replace("[PLAYER]", Siwa.getNameFromUUID(UUID.fromString(uuid))));
				}
			}
			if(Siwa.config.getBoolean("broadcast-warning"))Bukkit.getServer().broadcastMessage(Siwa.parseCC(Siwa.language.getString("broadcast").replace("[PLAYER]", player).replace("[REASON]", reason)));
		}
	}

	@Override
	public void run(){
		addWarning(player, reason, level, sender);
	}
}