package de.Neji3971.SimpleWarnings;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.bukkit.command.CommandSender;

public class AllWarningsThread implements Runnable{

	private final String		player;
	private final CommandSender	sender;

	public AllWarningsThread(String player, CommandSender sender){
		this.player = player;
		this.sender = sender;
	}

	private void getAllWarnings(String player, CommandSender sender){
		String uuid = Siwa.getUUIDFromName(player);
		if(uuid == null)sender.sendMessage(Siwa.parseCC(Siwa.language.getString("warn-added-fail")));
		else{
			String returns = Siwa.parseCC(Siwa.language.getString("sum-warns.none").replace("[PLAYER]", player));
			ResultSet res = Siwa.sql.executeQuery("SELECT id FROM warnings WHERE warned_player_id = " + Siwa.getIDOfPlayer(uuid));
			try{
				if(res.first()){
					returns = "";
					returns += getWarningsString(uuid, 1);
					for(int i = 2;; i++){
						if(res.next())returns += getWarningsString(uuid, i);
						else break;
					}
				}
			}catch(SQLException e){
				e.printStackTrace();
			}
			sender.sendMessage(Siwa.parseCC(returns));
		}
	}

	private String getWarningsString(String uuid, int ID){
		ResultSet res = Siwa.sql.executeQuery("SELECT reason,level,active FROM warnings WHERE warned_player_id = " + Siwa.getIDOfPlayer(uuid));
		while(true){
			try{
				if(res.getRow() == ID){
					String reason = res.getString("reason");
					int level = res.getInt("level");
					boolean active = res.getBoolean("active");
					return Siwa.parseCC(Siwa.language.getString("out-warn").replace("[ID]", new Integer(ID).toString()).replace("[PLAYER]", player)).replace("[REASON]", reason)
							.replace("[LEVEL]", new Integer(level).toString()).replace("[STATE]", new Boolean(active).toString());
				}else if(!res.next())return Siwa.parseCC(Siwa.language.getString("warn-not-exist"));
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	}

	@Override
	public void run(){
		getAllWarnings(player, sender);
	}
}