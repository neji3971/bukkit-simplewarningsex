package de.Neji3971.SimpleWarnings;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

public class ListenerHandler implements Listener{

	public Player	player;
	public String	UUID;
	public String	name;
	public int		ID;

	public ListenerHandler(){}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		if(Siwa.config.getBoolean("welcome-message") && !event.getPlayer().hasPermission("SimpleWarnings.override")){
			player.sendMessage(Siwa.parseCC(Siwa.language.getString("greeting").replace("[PLAYER]", name)));
			if(Siwa.getViolationLevel(ID) != 0) player.sendMessage(Siwa.parseCC(Siwa.language.getString("greet-level.more").replace("[LEVEL]", new Integer(Siwa.getViolationLevel(ID)).toString())));
			else player.sendMessage(Siwa.parseCC(Siwa.language.getString("greet-level.0")));
		}
		String warnings = getUnviewedWarnings(UUID);
		if(!warnings.equals("")){
			player.sendMessage(Siwa.parseCC(Siwa.language.getString("unread")));
			player.sendMessage(warnings);
		}
	}

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event){
		player = event.getPlayer();
		name = player.getName();
		UUID = Siwa.getUUIDFromName(name);
		ID = Siwa.getIDOfPlayer(UUID);
		if(Siwa.getViolationLevel(ID) >= Siwa.config.getInt("prevent-level") && !event.getPlayer().hasPermission("SimpleWarnings.override") && Siwa.config.getBoolean("join-prevent")){
			event.disallow(PlayerLoginEvent.Result.KICK_OTHER, Siwa.language.getString("no-join"));
		}
		String player_old = "";
		ResultSet res = Siwa.sql.executeQuery("SELECT `last known name` FROM players WHERE uuid = '" + UUID + "'");
		try{
			if(res.next())player_old = res.getString("last known name");
		}catch(SQLException e){
			e.printStackTrace();
		}
		if(!player_old.equals(name))Siwa.sql.executeUpdate("UPDATE players SET `last known name` = '" + name + "' WHERE uuid = '" + UUID + "'");
	}

	public String getUnviewedWarnings(String player){
		String returns = "";
		ResultSet res = Siwa.sql.executeQuery("SELECT reason,level FROM warnings WHERE warned_player_id = " + ID + " AND told = '0'");
		for(int i = 1;; i++){
			try{
				if(res.next()){
					returns += Siwa.parseCC(Siwa.language.getString("unread-warning").replace("[NUMBER]", new Integer(i).toString()).replace("[REASON]", res.getString("reason"))
							.replace("[LEVEL]", new Integer(res.getInt("level")).toString()));
				}else break;
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		Siwa.sql.executeUpdate("UPDATE warnings SET told = '1' WHERE told = '0' AND warned_player_id = '" + ID + "'");
		return returns;
	}
}